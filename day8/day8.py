#!/usr/bin/python

import sys
import re

RE_INSTRUCTION = re.compile(r'(acc|jmp|nop) ([\+-]\d+)$')

program = {}


class Instruction:
    def __init__(self, line):
        m = RE_INSTRUCTION.match(line)
        if m:
            self.original_opcode = m.group(1)
            self.opcode = self.original_opcode
            self.value = int(m.group(2))
        self.ran = 0

    def __str__(self):
        return f'Instruction[{self.opcode},{self.value},{self.ran}]'

    def mutate(self):
        if self.opcode == 'jmp':
            self.opcode = 'nop'
        elif self.opcode == 'nop':
            self.opcode = 'jmp'

    def reset(self):
        self.opcode = self.original_opcode
        self.ran = 0

    def exec(self, idx, acc):
        if self.ran > 0:
            raise RuntimeError(f'Infinite loop detected {self}')
        self.ran += 1
        if self.opcode == 'acc':
            return acc + self.value, idx + 1
        elif self.opcode == 'jmp':
            return acc, idx + self.value
        return acc, idx + 1


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        for idx, line in enumerate([x.rstrip() for x in f]):
            program[idx] = Instruction(line)
        print(f'part 1 {exec()}')
        print(f'part 2 {find_inf_loop()}')


def find_inf_loop():
    mut_idx, acc = 0, 0
    while mut_idx < len(program):
        for instruction in program.values():
            instruction.reset()
        try:
            program[mut_idx].mutate()
            idx, acc = 0, 0
            while idx < len(program):
                acc, idx = program[idx].exec(idx, acc)
            break
        except RuntimeError as e:
            # print(f'Mutation {mut_idx} failed {e}')
            mut_idx += 1
            continue
    return acc


def exec():
    idx, acc = 0, 0
    try:
        while idx < len(program):
            acc, idx = program[idx].exec(idx, acc)
    except RuntimeError as e:
        print(f'RuntimeError {e}')
    return acc


if __name__ == '__main__':
    main()
