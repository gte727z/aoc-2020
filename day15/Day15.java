import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

class Day15 {

  public HashMap<Integer, Integer> lastCalled = new HashMap<>();
  public Integer index = 1;
  public Integer nextNum = 0;
  public Integer target;

  public Day15(String data, Integer target) {
    this.target = target;
    run(data);
  }

  public static void main(String[] args) {
    if (args.length != 1) {
      Log.info("Please provide a file name");
      return;
    }
    try {
      File f = new File(args[0]);
      Scanner s = new Scanner(f);
      while (s.hasNextLine()) {
        String data = s.nextLine();
        long start = System.currentTimeMillis();
        Day15 d = new Day15(data, 2020);
        Log.info("Part 1 "+d.part1()+" "+(System.currentTimeMillis() - start)+"ms");

        start = System.currentTimeMillis();
        d = new Day15(data, 30000000);
        Log.info("Part 2 "+d.part1()+" "+(System.currentTimeMillis() - start)+"ms");
      }
      s.close();
    } catch (FileNotFoundException e) {
      Log.info(String.format("File not found : %s", args[0]));
    }
  }

  public void run(String s) {
    String[] startingNums = s.split(",");
    // Log.info("Starting nums " + s);
    for (String n : startingNums) {
      Integer x = Integer.parseInt(n);
      handle(x);
    }
    while (index < target) {
      handle(nextNum);
    }
  }

  public void handle(Integer x) {
    if (!lastCalled.containsKey(x)) {
      nextNum = 0;
    } else {
      nextNum = index - lastCalled.get(x);
    }
    lastCalled.put(x, index);
    index++;
  }

  public Integer part1() {
    return nextNum;
  }

  static class Log {
    public static void info(String s) {
      System.out.println(s);
    }
  }
}