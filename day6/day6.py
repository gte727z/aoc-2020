#!/usr/bin/python

import sys
import re


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        group = 1
        unique_answer_count = 0
        everyone_yes_count = 0
        answers = []
        for line in [x.rstrip() for x in f]:
            if line == "":
                unique_answer_count += get_unique_answers(group, ''.join(answers))
                everyone_yes_count += get_everyone_answered(group, answers)
                group += 1
                answers = []
            else:
                answers += [line]

        print(f'part 1 {unique_answer_count}')
        print(f'part 2 {everyone_yes_count}')


def get_unique_answers(group, answers):
    # print(f'group:{group} answers {answers}')
    answer_set = set()
    for char in answers:
        answer_set.add(char)
    return len(answer_set)

def get_everyone_answered(group, answer_groups):
    answer_count = {}
    sum = 0
    group_count = len(answer_groups)
    for answers in answer_groups:
        for a in answers:
            if a in answer_count:
                answer_count[a] += 1
            else:
                answer_count[a] = 1
    # print(f'group:{group} answers {answer_groups} answer counts {answer_count}')
    for count in answer_count.values():
        if count == group_count:
            sum += 1
    return sum



if __name__ == '__main__':
    main()
