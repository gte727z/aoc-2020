#!/usr/bin/python

import sys

loc = [0, 0]
facing = 'E'

waypoint = [1, 10]
ship = [0, 0]

def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        for line in [x.rstrip() for x in f]:
            globals()[line[0]](int(line[1:]))
            globals()[f'{line[0]}2'](int(line[1:]))
        print(f'part 1 {sum([abs(x) for x in loc])}')
        print(f'part 2 {sum([abs(x) for x in ship])}')


def N2(val):
    waypoint[0] += val

def S2(val):
    waypoint[0] -= val

def E2(val):
    waypoint[1] += val

def W2(val):
    waypoint[1] -= val

def L2(val):
    def adjust():
        global waypoint
        a, b = waypoint[0], waypoint[1]
        waypoint = [b, -1*a]
    while val > 0:
        adjust()
        val -= 90

def R2(val):
    def adjust():
        global waypoint
        a, b = waypoint[0], waypoint[1]
        waypoint = [-1*b, a]
    while val > 0:
        adjust()
        val -= 90


def F2(val):
    ship[0] += waypoint[0]*val
    ship[1] += waypoint[1]*val


def N(val):
    loc[0] += val

def S(val):
    loc[0] -= val

def E(val):
    loc[1] += val

def W(val):
    loc[1] -= val

def L(val):
    def adjust():
        global facing
        if facing == 'E':
            facing = 'N'
        elif facing == 'N':
            facing = 'W'
        elif facing == 'W':
            facing = 'S'
        elif facing == 'S':
            facing = 'E'
    while val > 0:
        adjust()
        val -= 90


def R(val):
    def adjust():
        global facing
        if facing == 'E':
            facing = 'S'
        elif facing == 'S':
            facing = 'W'
        elif facing == 'W':
            facing = 'N'
        elif facing == 'N':
            facing = 'E'
    while val > 0:
        adjust()
        val -= 90


def F(val):
    globals()[facing](val)




if __name__ == '__main__':
    main()
