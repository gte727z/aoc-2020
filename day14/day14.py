#!/usr/bin/python

from copy import deepcopy
import sys
import re

RE_INSTRUCTION = re.compile(r'(mask|mem)\[?(\d+)?\]? = ([\dX]+)$')

mask = 'init'
mem = {}

mask2 = 'init'
mem2 = {}


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        for idx, line in enumerate([x.rstrip() for x in f]):
            match = RE_INSTRUCTION.match(line)
            if match:
                exec_instruction(idx, match.group(1), match.group(2), match.group(3))
                exec_instruction2(match.group(1), match.group(2), match.group(3))
        print(f'part 1 {sum([x for x in mem.values()])}')
        print(f'part 2 {sum([x for x in mem2.values()])}')


def exec_instruction2(cmd, addr, val):
    global mask2, mem2

    def all_mem_addrs(x):
        all_addrs = ['']
        b_addr = f'{x:036b}'
        for i, m in enumerate(mask2):
            if m == '0':
                all_addrs = [(lambda x: x + str(b_addr[i]))(x) for x in all_addrs]
            elif m == '1':
                all_addrs = [(lambda x: x + '1')(x) for x in all_addrs]
            elif m == 'X':
                all_addrs_prime = []
                for a in all_addrs:
                    all_addrs_prime.append(a + '1')
                    all_addrs_prime.append(a + '0')
                all_addrs = all_addrs_prime
        return [int(''.join(x),2) for x in all_addrs]

    if cmd == 'mask':
        mask2 = val
    elif cmd == 'mem':
        for a in all_mem_addrs(int(addr)):
            mem2[a] = int(val)



def exec_instruction(idx, cmd, addr, val):
    global mask, mem

    # print(f'{idx} : {cmd} {addr} {val}')

    def apply_mask(x):
        bin_val = f'{x:036b}'
        masked_val = []
        for i, c in enumerate(bin_val):
            if mask[i] == 'X':
                masked_val.append(c)
            else:
                masked_val.append(mask[i])
        return int(''.join(masked_val),2)

    if cmd == 'mask':
        mask = val
    elif cmd == 'mem':
        mem[addr] = apply_mask(int(val))


if __name__ == '__main__':
    main()
