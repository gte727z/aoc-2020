#!/usr/bin/python

import sys
from datetime import datetime


def current_time_millis():
    return datetime.now().microsecond / 1000


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        data = [int(x.rstrip()) for x in f]
        jolt, one, three = 0, 0, 1
        data.sort()
        for i in range(0, len(data)):
            available = data[i:]
            matches = list(filter(lambda x: x <= jolt + 3, available))
            if matches[0] == jolt + 1:
                one += 1
            if matches[0] == jolt + 3:
                three += 1
            jolt = matches[0]
        print(f'part 1 {one}*{three}={one * three}')
        print(f'part 2 {find_possibilities(data)}')


def find_possibilities(data):
    branches = {0: list(filter(lambda x: 0 < x <= 3, data))}
    for i in range(0, len(data)):
        jolt = data[i]
        branches[data[i]] = list(filter(lambda x: jolt < x <= jolt + 3, data))
    keys = list(branches.keys())
    keys.reverse()
    for k in keys:
        if len(branches[k]) == 0:
            branches[k] = 1
        else:
            branches[k] = sum([branches[x] for x in branches[k]])
    return branches[0]


if __name__ == '__main__':
    main()
