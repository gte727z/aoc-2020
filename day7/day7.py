#!/usr/bin/python

import sys
import re

RE_BAG_RULE = re.compile(r'([a-z]+ [a-z]+) bags contain ([ 0-9a-z,]+)\.$')
RE_CHILDREN_LIST = re.compile(r' ?(\d+) ([a-z ]+) bags?$')


class BagRule:

    def __init__(self, str):
        self.parents = []
        self.children = []
        m = RE_BAG_RULE.match(str)
        if m:
            self.color = m.group(1)
            self.parse_children(m.group(2))

    def add_parent(self, color):
        self.parents.append(color)

    def parse_children(self, str):
        for child in str.split(','):
            m = RE_CHILDREN_LIST.match(child)
            if m:
                self.children.append((m.group(2), int(m.group(1))))

    def __str__(self):
        return f'color[{self.color}] parents{self.parents} children{self.children}'


bag_rules = {}


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        for line in [x.rstrip() for x in f]:
            rule = BagRule(line)
            bag_rules[rule.color] = rule
        for rule in bag_rules.values():
            for child in bag_rules[rule.color].children:
                bag_rules[child[0]].add_parent(rule.color)
        print(f'part 1 {len(get_all_parents(bag_rules["shiny gold"]))}')

        print(f'part 2 {count_all_children(bag_rules["shiny gold"], 1)}')

def get_all_parents(rule):
    allparents = set(rule.parents)
    for parent in rule.parents:
        allparents = allparents | get_all_parents(bag_rules[parent])
    return allparents


def count_all_children(rule, multiplier):
    count = 0
    for child in rule.children:
        count += (child[1] * multiplier)
        count += count_all_children(bag_rules[child[0]], child[1]*multiplier)
    return count


if __name__ == '__main__':
    main()
