#!/usr/bin/python

import sys
import re

REQUIRED_ATTRS = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

RE_FOUR_DIGITS = r'\d\d\d\d$'
RE_HEIGHT = r'(\d+)(in|cm)$'
RE_HAIR_COLOR = r'#[0-9a-f]{6}$'
RE_EYE_COLOR = r'amb|blu|brn|gry|grn|hzl|oth$'
RE_PASSPORT_ID = r'[0-9]{9}$'


class Passport:
    def __init__(self, id, data):
        self.id = id
        self.fields = {}
        # print(f'create passport:{id} from {data}')
        try:
            for d in data:
                kv = d.split(':')
                self.fields[kv[0]] = kv[1]
        except KeyError as e:
            raise Exception(f'issue parsing {data} {e}')

    def validate_fields_present(self):
        for attr in REQUIRED_ATTRS:
            if attr not in self.fields:
                # print(f'passport:{self.id} {self.fields} is missing {attr}')
                return False
        return True

    def validate(self):
        if not self.validate_fields_present():
            return False
        for attr in REQUIRED_ATTRS:
            try:
                getattr(Passport, f'validate_{attr}')(self.fields[attr])
            except ValueError as e:
                # print(f'passport:{self.id} {attr}:{self.fields[attr]} is invalid {e}')
                return False
        return True

    def __str__(self):
        return f'({self.id}:{self.fields})'

    @staticmethod
    def validate_pattern(pattern, value):
        p = re.compile(pattern)
        m = p.match(value)
        if m is not None:
            return True
        else:
            raise ValueError(f'Invalid {value}')

    @staticmethod
    def validate_year_between(value, y1, y2):
        p = re.compile(RE_FOUR_DIGITS)
        if p.match(value):
            year = int(value)
            if y1 <= year <= y2:
                return True
            else:
                raise ValueError(f'Out of range {value} {y1}-{y2}')
        else:
            raise ValueError(f'Invalid {value}')

    @staticmethod
    def validate_byr(value):
        return Passport.validate_year_between(value, 1920, 2002)

    @staticmethod
    def validate_iyr(value):
        return Passport.validate_year_between(value, 2010, 2020)

    @staticmethod
    def validate_eyr(value):
        return Passport.validate_year_between(value, 2020, 2030)

    @staticmethod
    def validate_hgt(value):
        p = re.compile(RE_HEIGHT)
        m = p.match(value)
        if m:
            height = int(m.group(1))
            unit = m.group(2)
            if unit == 'in' and 59 <= height <= 76:
                return True
            elif unit == 'cm' and 150 <= height <= 193:
                return True
        raise ValueError(f'Invalid height {value}')

    @staticmethod
    def validate_hcl(value):
        return Passport.validate_pattern(RE_HAIR_COLOR, value)

    @staticmethod
    def validate_ecl(value):
        return Passport.validate_pattern(RE_EYE_COLOR, value)

    @staticmethod
    def validate_pid(value):
        return Passport.validate_pattern(RE_PASSPORT_ID, value)


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        passport_data = []
        id = 1
        passports = []
        for line in [x.rstrip() for x in f]:
            if line == "" and len(passport_data):
                passports.append(Passport(id, passport_data))
                id += 1
                passport_data = []
            else:
                passport_data += line.split()

        print(f'total passports {id-1}')
        validate_part1(passports)
        validate_part2(passports)


def validate_part1(passports):
    valid = 0
    for p in passports:
        if p.validate_fields_present():
            valid += 1
    print(f'part 1 valid {valid}')


def validate_part2(passports):
    valid = 0
    for p in passports:
        if p.validate():
            valid += 1
    print(f'part 2 total valid {valid}')


if __name__ == '__main__':
    main()
