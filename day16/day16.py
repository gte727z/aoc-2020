#!/usr/bin/python

from functools import reduce
import sys
import re

RE_RULE = re.compile(r'([\w ]+): (\d+)-(\d+) or (\d+)-(\d+)$')

READING_RULES, MY_TICKET, NEARBY_TICKETS = 0, 1, 2

rules = []
my_ticket = None
nearby_tickets = []


class Rule:

    def __init__(self, line):
        self.index = None
        self.matches = set()
        match = RE_RULE.match(line)
        if match:
            self.name = match.group(1)
            self.r1, self.r2, self.r3, self.r4 = int(match.group(2)), int(match.group(3)), int(match.group(4)), int(
                match.group(5))
        else:
            raise RuntimeError(f'Bad rule match {line}')

    def validate(self, v):
        return self.r1 <= v <= self.r2 or self.r3 <= v <= self.r4

    def set_index(self, i):
        self.index = i

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f'Rule[{self.index}] {self.name} {self.r1}-{self.r2} {self.r3}-{self.r4}'


class Ticket:
    def __init__(self, line):
        self.values = [int(x) for x in line.split(',')]

    def __str__(self):
        return f'Ticket:{self.values}'

    def __repr__(self):
        return self.__str__()


def main():
    global rules, my_ticket, nearby_tickets
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        state = READING_RULES
        for idx, line in enumerate([x.rstrip() for x in f]):
            if line == '':
                continue
            elif line == 'your ticket:':
                state = MY_TICKET
            elif line == 'nearby tickets:':
                state = NEARBY_TICKETS
            elif state == READING_RULES:
                rules.append(Rule(line))
            elif state == NEARBY_TICKETS:
                nearby_tickets.append(Ticket(line))
            elif state == MY_TICKET:
                my_ticket = Ticket(line)
                nearby_tickets.append(my_ticket)

        def ticket_error_rate(t):
            errors = 0
            for v in t.values:
                errors += 0 if any([r.validate(v) for r in rules]) else v
            return errors

        def ticket_has_error(t):
            for v in t.values:
                if not any([r.validate(v) for r in rules]):
                    return True
            return False

        part1 = sum([ticket_error_rate(t) for t in nearby_tickets])
        print(f'part 1 {part1}')

        nearby_tickets = [t for t in nearby_tickets if not ticket_has_error(t)]
        for r in rules:
            for idx in range(0, len(my_ticket.values)):
                if all([r.validate(t.values[idx]) for t in nearby_tickets]):
                    r.matches.add(idx)
        rules.sort(key=lambda r: len(r.matches))

        used = set()
        while len([r for r in rules if r.index is None]) != 0:
            for r in [x for x in rules if x.index is None]:
                r.matches = r.matches - used
                if len(r.matches) == 1:
                    r.index = r.matches.pop()
                    used.add(r.index)

        part2 = reduce((lambda x, y: x * y), [my_ticket.values[r.index] for r in rules if r.name.startswith('departure')])

        print(f'part 2 {part2}')


if __name__ == '__main__':
    main()
