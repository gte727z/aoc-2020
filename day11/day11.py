#!/usr/bin/python

from copy import deepcopy
import sys
from time import time


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        original_map = [[char for char in line.rstrip()] for line in f]
        # print_map(original_map, 'original')

        start = time()
        p1 = count_p1(original_map)
        t = time() - start
        print(f'part 1 seat count {p1} {t}s')

        start = time()
        p2 = count_p2(original_map)
        t = time() - start
        print(f'part 2 seat count {p2} {t}s')


def count_p2(original_map):
    map = deepcopy(original_map)
    next_map = iterate_map(map, p2_neighbor_count, 5)
    i = 1
    while not equal(map, next_map):
        map = next_map
        next_map = iterate_map(map, p2_neighbor_count, 5)
        i += 1
    return seat_count(map)


def count_p1(original_map):
    map = deepcopy(original_map)
    next_map = iterate_map(map, p1_neighbor_count, 4)
    i = 1
    while not equal(map, next_map):
        map = next_map
        next_map = iterate_map(map, p1_neighbor_count, 4)
        i += 1
    return seat_count(map)


def seat_count(map):
    count = 0
    for line in map:
        for seat in line:
            if seat == '#':
                count += 1
    return count


def equal(left, right):
    for x, line in enumerate(left):
        for y, seat in enumerate(line):
            if seat != right[x][y]:
                return False
    return True


def iterate_map(map, count_function, acceptable_neighbors):
    new_map = deepcopy(map)
    for x, line in enumerate(map):
        for y, seat in enumerate(line):
            n = count_function(x, y, map)
            if seat == 'L' and n == 0:
                new_map[x][y] = '#'
            if seat == '#' and n >= acceptable_neighbors:
                new_map[x][y] = 'L'
    return new_map


def p2_neighbor_count(x, y, map):
    max_y = len(map[0])
    max_x = len(map)

    def see_in_direction(orig, movement):
        a = orig[0]
        b = orig[1]
        while True:
            a += movement[0]
            b += movement[1]
            if a < 0 or b < 0 or a >= max_x or b >= max_y:
                return 0
            elif map[a][b] == '#':
                return 1
            elif map[a][b] == 'L':
                return 0

    return sum([
        see_in_direction((x, y), (-1, -1)),
        see_in_direction((x, y), (-1, 0)),
        see_in_direction((x, y), (-1, 1)),
        see_in_direction((x, y), (0, 1)),
        see_in_direction((x, y), (1, 1)),
        see_in_direction((x, y), (1, 0)),
        see_in_direction((x, y), (1, -1)),
        see_in_direction((x, y), (0, -1)),
    ])


def p1_neighbor_count(x, y, map):
    max_y = len(map[0])
    max_x = len(map)

    def o(pair):
        if pair[0] < 0 or pair[1] < 0 or pair[0] >= max_x or pair[1] >= max_y:
            return 0
        return 1 if map[pair[0]][pair[1]] == '#' else 0

    return sum([
        o((x - 1, y - 1)),
        o((x - 1, y)),
        o((x - 1, y + 1)),
        o((x, y + 1)),
        o((x + 1, y + 1)),
        o((x + 1, y)),
        o((x + 1, y - 1)),
        o((x, y - 1)),
    ])


def print_map(map, title):
    print(f'{title}:')
    for line in map:
        print(f'{"".join(line)}')


if __name__ == '__main__':
    main()
