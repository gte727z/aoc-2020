#!/usr/bin/python

import sys

universe = {}

def point_2_str(p):
    return f'{p[0]},{p[1]},{p[2]},{p[3]}'


def str_2_point(p):
    return [int(x) for x in p.split(',')]


def main():
    global universe
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        for x, line in enumerate(f):
            for y, char in enumerate(line):
                if char == '.' or char == '#':
                    universe[point_2_str([x, y, 0, 0])] = char
        for iteration in range(0, 6):
            print(f'iter {iteration} count {len([x for x in universe.values() if x == "#"])}')
            add_new_points()
            update_points()

        print(f'part 2 {len([x for x in universe.values() if x == "#"])}')


def add_new_points():
    global universe
    keys = [x for x in universe.keys()]
    for p in keys:
        if universe[p] == '#':
            new_pts = all_adjacencies(str_2_point(p))
            for new_pt in [point_2_str(p) for p in new_pts]:
                if new_pt not in universe:
                    universe[new_pt] = '.'


def update_points():
    global universe
    new_universe = {}
    keys = [x for x in universe.keys()]
    for p in keys:
        adj = count_adjacencies(str_2_point(p))
        if universe[p] == '#':
            if not (adj == 2 or adj == 3):
                new_universe[p] = '.'
            else:
                new_universe[p] = '#'
        elif universe[p] == '.' and adj == 3:
            new_universe[p] = '#'
        else:
            new_universe[p] = '.'
    universe = new_universe


def count_adjacencies(p):
    global universe
    count = 0
    for a in all_adjacencies(p):
        adj = point_2_str(a)
        if adj in universe and universe[adj] == '#':
            count += 1
    return count


def all_adjacencies(p):
    aa = []
    for x in range (-1, 2):
        for y in range (-1, 2):
            for z in range (-1, 2):
                for w in range (-1, 2):
                    aa.append([p[0] + x, p[1] + y, p[2] + z, p[3] + w])

    return [x for x in aa if x != p]



if __name__ == '__main__':
    main()
