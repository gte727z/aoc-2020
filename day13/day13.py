#!/usr/bin/python

import sys

def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        busses = {}
        best_match = (0,0)
        part2 = 0
        for idx, line in enumerate([x.rstrip() for x in f]):
            if idx == 0:
                target = int(line)
            if idx == 1:
                part2 = calc_part_2([b for b in line.split(',')])
                for bus in [b for b in line.split(',') if b != 'x']:
                    val = int(bus)
                    busses[val] = (target % val) / val
                for k in busses.keys():
                    if busses[k] > best_match[1]:
                        best_match = (k, busses[k])
        best_match_bus = best_match[0]
        print(f'part 1 {(best_match_bus-(target % best_match_bus))*best_match_bus}')
        print(f'part 2 {part2}')


def calc_part_2(bus_list):
    bus_offsets = []
    for idx,bus in enumerate(bus_list):
        if bus != 'x':
            bus_offsets.append((int(bus), idx))
    bus_offsets = sorted(bus_offsets, key=lambda x: x[0], reverse=True)
    test_bus = bus_offsets[0]
    bus_offsets = bus_offsets[1:]
    print(f'test_bus {test_bus}')
    print(f'bus_offsets {bus_offsets}')
    i = 115512835594
    # i = int(100000000000000 / test_bus[0]) if test_bus[0] == 883 else 1
    matched = False
    while not matched:
        matched = True
        match_time = (i * test_bus[0]) - test_bus[1]
        i += 1
        if i % 1000000 == 0:
            print(f'check {match_time}')
        for offset in bus_offsets:
            if ((match_time + offset[1]) % offset[0]) != 0:
                matched = False
                break
    return match_time


def calc_part_2_try_2(bus_list):
    bus_offsets = []
    for idx,bus in enumerate(bus_list):
        if bus != 'x':
            bus_offsets.append((int(bus), idx))
    print(f'bus_offsets {bus_offsets}')
    test_bus = bus_offsets[0]
    match_time = 0
    for offset in bus_offsets:
        if match_time == 0:
            match_time = offset[0]
    return match_time



if __name__ == '__main__':
    main()
