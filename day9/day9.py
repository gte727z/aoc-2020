#!/usr/bin/python

import sys
from datetime import datetime


PREAMBLE = 25
current_time_millis = lambda: datetime.now().microsecond / 1000


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        data = [int(x.rstrip()) for x in f]

        start = current_time_millis()
        invalid = find_invalid(data)
        time = current_time_millis() - start
        print(f'part 1 {invalid} {time}ms')

        start = current_time_millis()
        weakness = find_weakness(invalid, data)
        time = current_time_millis() - start
        print(f'part 2 {weakness} {time}ms')


def find_weakness(code, data):
    i = 0
    while i < len(data):
        j, match = i, 0
        while match < code:
            contiguous = data[i:j]
            match = sum(contiguous)
            if match == code:
                contiguous.sort()
                return contiguous[0] + contiguous[len(contiguous)-1]
            j += 1
        i += 1
    return []


def find_invalid(data):
    nums = []
    for d in data:
        match = False
        if len(nums) == PREAMBLE:
            for n in nums:
                target = d - n
                if target != n and target in nums:
                    nums.pop(0)
                    nums.append(d)
                    match = True
                    break
            if not match:
                return d
        else:
            nums.append(d)
    return 0


if __name__ == '__main__':
    main()
