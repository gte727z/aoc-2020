#!/usr/bin/python

import sys
import re


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        highest = 0
        all_seat_ids = [compute_seat_id(x.rstrip()) for x in f]
        all_seat_ids.sort()
        sid = all_seat_ids[0]
        while True:
            sid +=1
            if sid not in all_seat_ids:
                break
        print(f'your seat {sid}')
        print(f'highest seat {all_seat_ids[len(all_seat_ids)-1]}')


def compute_seat_id(seat_code):
    row = 0
    col = 0
    row_half = 64
    col_half = 4
    for i in range(7):
        if seat_code[i] == 'B':
            row += row_half
        row_half /= 2
    for i in range(7,10):
        if seat_code[i] == 'R':
            col += col_half
        col_half /= 2
    seat_id = (row*8)+col
    # print(f'seat {seat_code} row {row} col {col} id {seat_id}')
    return seat_id


if __name__ == '__main__':
    main()
