#!/usr/bin/python

import sys

class Position:
    def __init__(self, max_width):
        self.x = 0
        self.y = 0
        self.max_width = max_width

    def move(self, slope):
        self.x = (self.x + slope[0]) % self.max_width
        self.y += slope[1]

    def __str__(self):
        return f'({self.x},{self.y})'



class Map:
    def __init__(self, f):
        self.rows = []
        self.width, self.length = 0, 0
        self.parse_input(f)

    def parse_input(self, f):
        self.rows = [x for x in f if clean(x)]
        self.width = len(self.rows[0]) - 1
        self.height = len(self.rows)

    def traverse_for_trees(self, slope):
        trees = 0
        position = Position(self.width)
        while position.y < self.height:
            if self.is_tree(position):
                trees += 1
            position.move(slope)
        return trees

    def is_tree(self, p):
        return self.rows[p.y][p.x] == '#'


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        map = Map(f)
        print(f'Part 1 : {map.traverse_for_trees((3,1))}')
        product = map.traverse_for_trees((1,1))
        product *= map.traverse_for_trees((3,1))
        product *= map.traverse_for_trees((5,1))
        product *= map.traverse_for_trees((7,1))
        product *= map.traverse_for_trees((1,2))
        print(f'Part 2 : {product}')


def clean(x):
    return x is not None and x is not '\n'


if __name__ == '__main__':
    main()
