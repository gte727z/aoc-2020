#!/usr/bin/python

import re
import sys


RE_PATTERN = r'(\d+)-(\d+) ([a-z]): ([a-z]+)'


class PasswordValidator:
    def __init__(self, low, hi, char, password):
        self.low = int(low)
        self.hi = int(hi)
        self.char = char
        self.password = password

    def validate(self):
        count = 0
        for c in self.password:
            if c == self.char:
                count += 1
        return (count >= self.low) and (count <= self.hi)


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        print(f'valid passwords: {process_passwords(parse_input(f))}')


def parse_input(line):
    def clean(x):
        return x is not None and x is not '\n'
    return [x for x in line if clean(x)]


def process_passwords(passwords):
    valid = 0
    matcher = re.compile(RE_PATTERN)
    for p in passwords:
        match = matcher.match(p)
        if PasswordValidator(match.group(1), match.group(2), match.group(3), match.group(4)).validate():
            valid += 1
    return valid


if __name__ == '__main__':
    main()
