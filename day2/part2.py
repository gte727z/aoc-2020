#!/usr/bin/python

import re
import sys


RE_PATTERN = r'(\d+)-(\d+) ([a-z]): ([a-z]+)'


class PasswordValidator:
    def __init__(self, pos1, pos2, char, password):
        self.pos1 = int(pos1)-1
        self.pos2 = int(pos2)-1
        self.char = char
        self.password = password

    def validate(self):
        first = self.password[self.pos1] == self.char
        second = self.password[self.pos2] == self.char
        return first != second


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        print(f'valid passwords: {process_passwords(parse_input(f))}')


def parse_input(line):
    def clean(x):
        return x is not None and x is not '\n'
    return [x for x in line if clean(x)]


def process_passwords(passwords):
    valid = 0
    matcher = re.compile(RE_PATTERN)
    for p in passwords:
        match = matcher.match(p)
        if PasswordValidator(match.group(1), match.group(2), match.group(3), match.group(4)).validate():
            valid += 1
    return valid


if __name__ == '__main__':
    main()
