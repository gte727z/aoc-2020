#!/usr/bin/python

import sys

TOTAL = 2020


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return 0
    with open(sys.argv[1], 'r') as f:
        print(f'{find_match([int(x) for x in f if clean(x)])}')


def clean(x):
    return x is not None and x is not '\n'


def find_match(numbers):
    for n in numbers:
        target = TOTAL - n
        if target in numbers:
            return target * n
    return None


if __name__ == '__main__':
    main()
