#!/usr/bin/python

import sys

TOTAL = 2020


def main():
    if len(sys.argv) == 1:
        print('Please provide file name')
        return
    with open(sys.argv[1], 'r') as f:
        values = [int(x) for x in f if clean(x)]
        for first in values:
            targets = possible_matches(values, first, [first])
            for second in targets:
                third = possible_matches(targets, first + second, [first, second], True)
                if len(third) == 1:
                    print(f'{first}*{second}*{third[0]} = {first * second * third[0]}')
                    return


def clean(x):
    return x is not None and x is not '\n'


def possible_matches(values, v, excluded, exact=False):
    target = TOTAL - v

    def fn(x):
        if exact:
            return x == target
        else:
            return x <= target and x not in excluded

    return [x for x in filter(fn, values)]


if __name__ == '__main__':
    main()
